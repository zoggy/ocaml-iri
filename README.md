# OCaml-iri -- IRI parser, printer and more

[🌐 OCaml-iri homepage](https://zoggy.frama.io/ocaml-iri/)

OCaml implementation of Internationalized Resource Identifiers (IRIs, defined
in [RFC 3987](http://tools.ietf.org/html/rfc3987).

This implementation does not depend on regular expression library.
Is is implemented using [Sedlex](https://github.com/ocaml-community/sedlex)
thus it will be usable in Javascript (with Js_of_ocaml).

## Documentation

Documentation is available [here](https://zoggy.frama.io/ocaml-iri/refdoc/iri/IRI/index.html).

## Development

Development is hosted on [Framagit](https://framagit.org/zoggy/ocaml-iri).

OCaml-IRI is released under LGPL3 license.

## Installation

The `iri` package is installable with opam:

```sh
$ opam install iri
```

Current state of OCaml-iri can be installed with:

```sh
$ opam pin add iri git@framagit.org:zoggy/ocaml-iri.git
```

## Releases

- [1.0.0](https://zoggy.frama.io/ocaml-iri/releases/ocaml-iri-1.0.0.tar.bz2) (2024/02/02)
- [0.7.0](https://zoggy.frama.io/ocaml-iri/releases/ocaml-iri-0.7.0.tar.bz2) (2023/07/10)
- [0.6.0](https://zoggy.frama.io/ocaml-iri/releases/ocaml-iri-0.6.0.tar.bz2) (2021/12/10)
- [0.5.0](https://zoggy.frama.io/ocaml-iri/releases/ocaml-iri-0.5.0.tar.gz) (2021/05/04)
- [0.4.0](https://zoggy.frama.io/ocaml-iri/releases/ocaml-iri-0.4.0.tar.gz) (2016/11/30)
- [0.3.0](https://zoggy.frama.io/ocaml-iri/releases/ocaml-iri-0.3.0.tar.gz) (2016/05/03)
- [0.2.0](https://zoggy.frama.io/ocaml-iri/releases/ocaml-iri-0.2.0.tar.gz) (2016/03/22)
- [0.1.0](https://zoggy.frama.io/ocaml-iri/releases/ocaml-iri-0.1.0.tar.gz) (2016/01/26)
