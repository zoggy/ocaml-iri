
SHELL := /bin/bash
#
all:
	dune build

install:
	dune build @install
	dune install

doc: all
	dune build @doc

webdoc: doc
	rm -fr public/refdoc
	cp -r _build/default/_doc/_html public/refdoc
	tail -n +2 README.md | pandoc -f markdown -t html -s --metadata title="OCaml-iri" > public/index.html

# archive :
###########
archive:
	$(eval VERSION=`git describe | cut -d'-' -f 1`)
	git archive --worktree-attributes --prefix=ocaml-iri-$(VERSION)/ $(VERSION) | bzip2 > public/releases/ocaml-iri-$(VERSION).tar.bz2


# Cleaning :
############
clean:
	dune clean

# headers :
###########
HEADFILES:=$(shell ls Makefile lib/*.ml{,i} tests/*.ml)
.PHONY: headers noheaders
headers:
	echo $(HEADFILES)
	headache -h .header -c .headache_config $(HEADFILES)

noheaders:
	headache -r -c .headache_config $(HEADFILES)

